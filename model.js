export const CITY_LIST = await fetch(`CITY_LIST.json`).then(res => res.json());
const API_KEY = '1fb06e44665d058101ed448632caaa9b' ;


export async function getWeatherData(currentID){

    try{
    const data = CITY_LIST.find(el=> el.id==currentID );
    const hourlyData = await fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${data.coord.lat}&lon=${data.coord.lon}&exclude=minutely,daily,alert&appid=${API_KEY}`).then(res => res.json());
    return [data,hourlyData];
    }
    catch(err)
    {
        alert('Something Went Wrong');
    }   
};


export function changeToAmPm(num)
{   
if(num===0||num===24)
{
 return `12 AM`   
}
else if(num<12)
{
return `${num} AM`
}
else if(num===12)
{
  return `12 PM`
} 
else if(num>12||num<24)
{
return `${num-12} PM`
}
};