
import * as view from "./view.js";
import * as model from "./model.js";

let currentID ;
const formCityList = document.querySelector('.city-list') ;

view.init();

if(localStorage.getItem('Id'))
{
  currentID = localStorage.getItem('Id');
  view.dropDown.value = currentID ;
}
else
{
  currentID = view.dropDown.value ;
}

const weatherData= await model.getWeatherData(currentID);
view.viewInfo(weatherData);

formCityList.addEventListener('submit',async function(e){
e.preventDefault();
let currentID = view.dropDown.value ;

const weatherData= await model.getWeatherData(currentID);
view.viewInfo(weatherData);
localStorage.setItem('Id',`${currentID}`);
});

















  









