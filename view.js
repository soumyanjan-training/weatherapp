import * as model from "./model.js";

export const dropDown = document.querySelector(".drop_down");
export const hourlyView = document.querySelector(".hourly-view");
const month = [
  "Jan",
  "Feb",
  "March",
  "April",
  "May",
  "June",
  "July",
  "Aug",
  "Sept",
  "Oct",
  "Nov",
  "Dec"
];
const weatherView = document.querySelector(".weather-view");
const dateView = document.querySelector(".date");
const hourlyBody = document.querySelector(".hourly-body");
const chartContainer = document.querySelector(".chart-container");
const date = new Date();

let myChart;
let curTime;
let day = "Today";
let name;

export function viewInfo(data) {
  const cityData = data[0];
  const weatherData = data[1];
  console.log(weatherData);

  viewCurrentData(cityData.name, weatherData.current, "Now");
  name = cityData.name;

  chartContainer.insertAdjacentHTML(
    "beforeend",
    `
            <canvas id="myChart" width="100px" height="350"></canvas>
    `
  );

  let currentHour = date.getHours();

  const today = 24 - currentHour;
  const todayData = weatherData.hourly.slice(0, today - 1);
  const tomorrowData = weatherData.hourly.slice(today, today + 24);
  const dayAfterTomorrowData = weatherData.hourly.slice(today + 24, 47);

  viewHourlyData(day, todayData, tomorrowData, weatherData.current);

  hourlyView.addEventListener("click", function(e) {
    let id;
    const removeHighlight = function() {
      document
        .querySelectorAll(".hourly")
        .forEach(el => el.classList.remove("highlighted"));
    };
    if (e.target.parentElement) {
      if (e.target.parentElement.parentElement) {
        if (e.target.parentElement.parentElement.id)
          id = e.target.parentElement.parentElement.id;
      }
      if (e.target.parentElement.id) id = e.target.parentElement.id;
    }
    if (e.target.id) id = e.target.id;
    if (id === "current") {
      viewCurrentData(cityData.name, weatherData.current, "Now");
      removeHighlight();
      document.querySelector(`.precipitation-${id}`).classList.add("highlighted");
    }
    if (!isNaN(id)) {
      removeHighlight();
      let currentData = todayData.find(element => element.dt == `${id}`);
      if (currentData == undefined)
        currentData = tomorrowData.find(element => element.dt == `${id}`);
      viewCurrentData(cityData.name, currentData, currentData.time);
      document
        .querySelector(`.precipitation-${id}`)
        .classList.add("highlighted");
    }
  });
}

function viewCurrentData(name, currentData, time) {
  const weatherBody = document.querySelector(".body");
  weatherBody.innerHTML = "";

  const currentHTML = `
     <p class="city">${name}</p>
     <h2 class="Time">${time}</h2>
     <p class="temperature">${Math.ceil(
       currentData.temp - 273.15
     )}<span class="iconify" data-icon="wi:degrees"></span>C</p>
     <p class="temperature">Feels like ${Math.ceil(
       currentData.temp - 273.15
     )}<span class="iconify" data-icon="wi:degrees"></span>C . ${
    currentData.weather[0].main
  }. ${currentData.weather[0].description}</p>
     <p class="precipitation" > ${precipitation(
       currentData.clouds
     )} chances of raining </p>
<table class="weather_info">
        <tr>
                <td>
                 <span class="iconify" data-icon="whh:speed"></span>
 : ${currentData.wind_speed}m/s 
                </td>
                <td>
                pressure : ${currentData.pressure}hPa 
                </td>
            </tr>
             <tr> <td>
                Humidity : ${currentData.humidity}
                </td>
                <td>Visibility:
                           ${currentData.visibility / 1000} Km
                </td>
            </tr>
            
</table></div>
`;

  weatherBody.insertAdjacentHTML("afterbegin", currentHTML);
  const body = document.querySelector("body");
  if (currentData.clouds < 50) {
    body.style.backgroundImage = 'url("sunnyWeather.png")';
  } else {
    body.style.backgroundImage = 'url("rain.jpg")';
  }
}

function viewHourlyData(day, todayData, tomorrowData, currentHourData) {
  let currentHour;
  hourlyBody.innerHTML = "";

  let timeLabels = [];
  let tempData = [];
  let currentData;

  if (day === "Today") {
    currentData = todayData;
    currentHour = date.getHours();
    viewHourlyTemplate(currentHourData,'current','Now');
  } 
  else if (day === "Tomorrow") {
    currentData = tomorrowData;
    currentHour = -1;
  }

  dayTemplate(day);
  currentData.forEach(el => {
    currentHour++;
    el.time = `${day} ${model.changeToAmPm(currentHour)}`;
    timeLabels.push(model.changeToAmPm(currentHour));
    tempData.push(Math.ceil(el.temp - 273.15));
    let i = el.dt;
    viewHourlyTemplate(el,i,model.changeToAmPm(currentHour));
    
  });

  viewChart(timeLabels, tempData);

  if (day === "Tomorrow") {
    document.querySelector(".hourly").classList.add("highlighted");
    viewCurrentData(name, currentData[0], currentData[0].time);
    day="Today";
  }
  else if (day === "Today") {
    document.querySelector(".hourly").classList.add("highlighted");
    viewCurrentData(name, currentHourData, "Now");
    day="Tomorrow";
  }


  document.querySelector(".next-day").addEventListener("click", function(e) {
    e.preventDefault();
    viewHourlyData(day, todayData, tomorrowData, currentHourData);
  });
}

function dayTemplate(day) {
  let currentDay;
  const hourlyNav = document.querySelector(".hourly-nav");
  hourlyNav.innerHTML = "";
  if (day == "Today") {
    currentDay =
      '<h2 class="day">Today </h2><button class="next-day">Tomorrow <span class="iconify" data-icon="akar-icons:arrow-right"></span></button>';
  }
  if (day == "Tomorrow") {
    currentDay =
      '<h2 class="day">Tomorrow </h2><button class="next-day">Today <span class="iconify" data-icon="akar-icons:arrow-right"></span></button>';
  }
  hourlyNav.insertAdjacentHTML("afterbegin", currentDay);
}

function viewChart(labels, dataSet) {
  const data = {
    labels: labels,
    datasets: [
      {
        label: "Temperature in Celsius",
        backgroundColor: "rgb(38, 59, 248)",
        borderColor: "rgba(75, 75, 75,0.5)",
        data: dataSet
      }
    ]
  };
  const config = {
    type: "line",
    data: data,
    options: {
      responsive: true,
      maintainAspectRatio: false
    }
  };
  let ctx = document.getElementById("myChart").getContext("2d");
  ctx.canvas.width = 300;
  ctx.canvas.height = 300;
  if (myChart) {
    myChart.destroy();
  }
  myChart = new Chart(ctx, config);
}

export function init() {
  const dropDown = document.querySelector(".drop_down");
  model.CITY_LIST.forEach(element => {
    const html = `<option value = ${element.id} selected>${element.name}</option>`;
    dropDown.insertAdjacentHTML("afterbegin", html);
  });
  let dateTime = `${
    month[date.getMonth()]
  } ${date.getDate()}, ${date.getHours()}:${date.getMinutes()}`;
  dateView.insertAdjacentHTML("beforeend", dateTime);
}

function precipitation(cloud) {
  if (cloud > 50) {
    return `<span class="iconify" data-icon="wi:day-rain-mix"></span> ${cloud}%`;
  } else
    return `<span class="iconify" data-icon="wi:day-cloudy-high"></span> ${cloud}%`;
}

function viewHourlyTemplate(hourlyData,id,time){

const hourlyHTML = `
<div class="hourly precipitation-${id}" id="${id}" >
<div class="time" id="${id}">${time}</div>
<div class = "hourly-content hourly-content-${id} id="${id}" >
<p class="temperature">${Math.ceil(
      hourlyData.temp - 273.15
    )}<span class="iconify" data-icon="wi:degrees"></span>C</p>
<p class="precipitation-hr">${precipitation(hourlyData.clouds)}</p>
</div>
</div>
`;

if(id==='current')
hourlyBody.insertAdjacentHTML("afterbegin", hourlyHTML);
else
hourlyBody.insertAdjacentHTML("beforeend", hourlyHTML);

    let hourlyElCurr = document.querySelector(`.precipitation-${id}`);
    if (Number(hourlyData.clouds) < 50) {
      hourlyElCurr.style.backgroundImage = 'url("sunnyWeather-small.png")';
      hourlyElCurr.style.color = "#000";
    } else {
      hourlyElCurr.style.backgroundImage =
        'linear-gradient(rgba(255, 255, 255, 0.1),rgba(255, 255, 255, 0.1)),url("rain-1.jpeg")';
      hourlyElCurr.style.color = "#fff";
    }
 ;
} 
